package com.example.ejercicioactivities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ConfirmacionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion);
        this.cargarControl("txtNombre","nombre");
        this.cargarControl("fechaNacimiento","fechaNacimiento");
        this.cargarControl("telefono","telefono");
        this.cargarControl("email","email");
        this.cargarControl("descripcion","descripcion");
    }
    private void cargarControl(String nombreParam, String nombreControl) {
        Bundle parametros = getIntent().getExtras();
        String valor = parametros.getString(nombreParam);
        int resId = getResources().getIdentifier(nombreControl, "id", getPackageName());
        TextView text = findViewById(resId);
        text.setText(valor);
    }
    public void editar(android.view.View vista){
        super.onBackPressed();
    }
}