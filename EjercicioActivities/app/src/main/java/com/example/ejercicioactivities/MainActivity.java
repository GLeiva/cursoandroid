package com.example.ejercicioactivities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String curDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CalendarView calendario = findViewById(R.id.fechaNacimiento);
        calendario.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                curDate = day + "/" + month + "/" + year;
            }
        });
    }
    public void siguiente(android.view.View vista){
        Intent IntentConfirmacion = new Intent(this, ConfirmacionActivity.class);
        this.cargarParametro(IntentConfirmacion,"txtNombre");
        //this.cargarParametro(IntentConfirmacion,"fechaNacimiento");
        this.cargarParametro(IntentConfirmacion,"descripcion");
        this.cargarParametro(IntentConfirmacion,"telefono");
        this.cargarParametro(IntentConfirmacion,"email");
        IntentConfirmacion.putExtra("fechaNacimiento", curDate);
        //this.cargarParametro(IntentConfirmacion,"descripcion");
        //Toast.makeText(this, txtNombre.getText().toString(), Toast.LENGTH_SHORT).show();
        startActivity(IntentConfirmacion);
    }
    private void cargarParametro(Intent intent, String nombreControl){
        int resId = getResources().getIdentifier(nombreControl,"id",getPackageName());
        EditText text = findViewById(resId);
        intent.putExtra(nombreControl,text.getText().toString());
    }
}